# RequestManager

 The RequestManager framework provides a simple and flexible way to make network requests and handle responses in Swift. It supports fetching data that conforms to the Decodable protocol as well as handling requests where no response data is expected.
# Features

    Asynchronous network requests
   Support for Decodable response data
    Simple handling of requests with no response data

# Swift Package Manager

 Add the following to your Package.swift:

 dependencies: [
  .package(url: "https://github.com/yourusername/RequestManager.git", from: "1.0.0")
 ]
